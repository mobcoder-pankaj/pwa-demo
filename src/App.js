import { useState, useEffect } from 'react';
import logo from './logo.svg';
import './App.css';

function App() {

  const [data, setData] = useState()

  useEffect(() => {
    fetch('https://dummyjson.com/users')
      .then(res => res.json())
      .then(json => {
        console.log(json?.users)
        setData(json?.users)
      })

  }, [])


  return (
    <div className="App">
      <h1>User list</h1>
      {
        data?.map((item) => {
          return (
            <div key={item?.id} className="main_container">
              <img src={item?.image} alt="user" />
              <h1>{item?.firstName}</h1>
            </div>
          )
        })
      }
    </div>
  );
}

export default App;
