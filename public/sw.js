const CACHE_NAME = "pwa_V1"
const CACHE_PATH = ["index.js"]
const self = this

self.addEventListener('install', (event) => {
    event.waitUntil(
        caches.open(CACHE_NAME).then(cache => {
            cache.addAll(CACHE_PATH)
                .then(() => {
                    self.skipWaiting()
                })
        }).catch(err => {
            console.log("error in install caches", err)
        })
    )
})


self.addEventListener('activate', (event) => {
    console.log("Activated")
    event.waitUntil(
        caches.keys().then(cacheName => {
            return Promise.all(
                cacheName.map(
                    cache => {
                        if (cache != cacheName) {
                            return caches.delete(cache)
                        }
                    })
            )
        })
    )

})


self.addEventListener("fetch", (event) => {
    console.log(`Handling fetch event for ${event.request.url}`);
  
    event.respondWith(
      caches.match(event.request).then((response) => {
        if (response) {
          console.log("Found response in cache:", response);
          return response;
        }
        console.log("No response found in cache. About to fetch from network…");
  
        return fetch(event.request)
          .then((response) => {
            console.log("Response from network is:", response);
  
            return response;
          })
          .catch((error) => {
            console.error(`Fetching failed: ${error}`);
            throw error;
          });
      })
    );
  });
